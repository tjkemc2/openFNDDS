package com.tjk104.openfndds;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tjk104.openfndds.database.FoodNutrient;

import java.util.List;

public class NutrientListAdapter extends RecyclerView.Adapter<NutrientListAdapter.NutrientListViewHolder> {
    private List<FoodNutrient> mNutrientList;
    private final LayoutInflater mInflater;

    public static class NutrientListViewHolder extends RecyclerView.ViewHolder{
        private final TextView nutrientName;
        private final TextView nutrientAmount;

        public NutrientListViewHolder(@NonNull LinearLayout itemView) {
            super(itemView);
            nutrientName = itemView.findViewById(R.id.nutrient_name);
            nutrientAmount = itemView.findViewById(R.id.nutrient_amount);
        }

        public void setNutrientName(String name){
            nutrientName.setText(name);
        }

        public void setNutrientAmount(String amount){
            nutrientAmount.setText(amount);
        }
    }

    NutrientListAdapter(Context context){ mInflater = LayoutInflater.from(context); }

    @NonNull
    @Override
    public NutrientListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new NutrientListViewHolder((LinearLayout) mInflater.inflate(R.layout.nutrient_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull NutrientListViewHolder holder, int position) {
        Resources res = MainActivity.getContext().getResources();
        FoodNutrient entry = mNutrientList.get(position);
        String nameString = res.getString(R.string.nutrient_name, entry.description);
        String amountString = res.getString(R.string.nutrient_amount, entry.amount, entry.unitName);
        holder.setNutrientName(nameString);
        holder.setNutrientAmount(amountString);
    }

    @Override
    public int getItemCount() {
        return mNutrientList == null ? 0 : mNutrientList.size();
    }

    public void updateList(List<FoodNutrient> foodNutrients){
        mNutrientList = foodNutrients;
        notifyDataSetChanged();
    }
}
