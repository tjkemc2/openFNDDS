package com.tjk104.openfndds;

import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.tabs.TabLayout;

public class InfoActivity extends AppCompatActivity {
    private TextView contentView;
    private enum tabs {INFO, HELP, LICENSE}

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        TabLayout tabLayout = findViewById(R.id.info_tab_layout);
        contentView = findViewById(R.id.tab_content);

        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getTag() instanceof tabs){
                    switch((tabs)tab.getTag()){
                        case INFO:
                            contentView.setText(R.string.info_body);
                            break;
                        case HELP:
                            contentView.setText(R.string.help_body);
                            break;
                        case LICENSE:
                            contentView.setText(R.string.license_body);
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                // Don't need to do anything...
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                // Don't need to do anything here either...
            }
        });

        TabLayout.Tab infoTab = tabLayout.newTab();
        infoTab.setText("Info");
        infoTab.setTag(tabs.INFO);
        tabLayout.addTab(infoTab);

        TabLayout.Tab helpTab = tabLayout.newTab();
        helpTab.setText("Helpful Tips");
        helpTab.setTag(tabs.HELP);
        tabLayout.addTab(helpTab);

        TabLayout.Tab licenseTab = tabLayout.newTab();
        licenseTab.setText("Licenses");
        licenseTab.setTag(tabs.LICENSE);
        tabLayout.addTab(licenseTab);

        tabLayout.selectTab(infoTab, true);
    }
}