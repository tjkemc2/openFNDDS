package com.tjk104.openfndds;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tjk104.openfndds.database.FoodDatabase;
import com.tjk104.openfndds.database.FoodItem;
import com.tjk104.openfndds.database.FoodItemDao;

import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {
    // This isn't ideal but I can't figure out how else to access app resources from some no-context classes
    @SuppressLint("StaticFieldLeak")
    protected static MainActivity instance;
    private static final int NUM_THREADS = 4;
    private final ExecutorService executorService = Executors.newFixedThreadPool(NUM_THREADS);
    private FoodItemDao mFoodItemDao;
    private FoodListAdapter mAdapter;
    protected List<FoodItem> foodItems;
    private View loadingSpinner;
    private SharedPreferences prefs;

    private SearchView mSearchView;

    private boolean filteringFavorites = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        instance = this;
        FoodDatabase db = FoodDatabase.getDatabase(this);
        mFoodItemDao = db.foodItemDao();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadingSpinner = findViewById(R.id.main_view_loading_overlay);
        RecyclerView recyclerView = findViewById(R.id.food_item_recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new FoodListAdapter(this);
        recyclerView.setAdapter(mAdapter);
        loadingSpinner.setVisibility(View.VISIBLE);
        executorService.execute(() -> {
            try {
                db.getOpenHelper().getWritableDatabase();
                db.waitForExecutor();
                runOnUiThread(() -> updateLoadingBar("Finishing up..."));
                foodItems = mFoodItemDao.getAll();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                runOnUiThread(() -> {
                    mAdapter.updateList(foodItems);
                    loadingSpinner.setVisibility(View.GONE);
                });
            }
        });
        prefs = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        AppCompatDelegate.setDefaultNightMode(prefs.getInt("DayNightMode", AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_view_menu, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final MenuItem openComparisonView = menu.findItem(R.id.action_launch_comparison_activity);
        openComparisonView.setOnMenuItemClickListener(item -> {
            Intent intent = new Intent(this, ComparisonActivity.class);
            this.startActivity(intent);
            return true;
        });
        final MenuItem filterFavorites = menu.findItem(R.id.action_filter_favorites);
        filterFavorites.setOnMenuItemClickListener(item -> {
            filterFavorites();
            return true;
        });
        final MenuItem openHelp = menu.findItem(R.id.action_launch_info_activity);
        openHelp.setOnMenuItemClickListener(item -> {
            Intent intent = new Intent(this, InfoActivity.class);
            this.startActivity(intent);
            return true;
        });
        final MenuItem toggleNightMode = menu.findItem(R.id.setting_night_mode);
        toggleNightMode.setOnMenuItemClickListener(item -> {
            cycleDayNight();
            return true;
        });
        mSearchView = (SearchView) searchItem.getActionView();
        mSearchView.setOnQueryTextListener(this);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        if(newText.equals("")){
            mAdapter.updateList(foodItems);
        }
        return false;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        List<FoodItem> searchResults = foodItems.stream()
                .filter(e -> {
                    String name = e.name.toLowerCase();
                    return Arrays.stream(query.toLowerCase().split("\\s+")).allMatch(name::contains);
                }).collect(Collectors.toList());
        mAdapter.updateList(searchResults);
        return false;
    }

    @Override
    public void onBackPressed() {
        if(!mSearchView.isActivated()){
            mSearchView.setQuery("", true);
            mSearchView.onActionViewCollapsed();
            filterFavorites(false);
        } else if(filteringFavorites){
            filterFavorites(false);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        if(filteringFavorites){
            filterFavorites(true);
        }
        super.onResume();
    }

    public static Context getContext(){
        return instance.getApplicationContext();
    }

    public void updateRecyclerView(View view) {
        executorService.execute(() -> {
            foodItems = mFoodItemDao.getAll();
            runOnUiThread(() -> mAdapter.updateList(foodItems));
        });
    }

    public static void updateLoadingBar(String text){
        TextView loadingText = instance.loadingSpinner.findViewById(R.id.loading_screen_textview);
        loadingText.post(() -> loadingText.setText(text));
    }

    private void filterFavorites(){
        if(!mSearchView.isActivated()){
            mSearchView.setQuery("", true);
            mSearchView.onActionViewCollapsed();
        }
        if(!filteringFavorites){
            Set<Integer> favoritesList = Arrays.stream(prefs.getString("Favorites", "").split(",")).filter(StringUtils::isNumeric).map(Integer::parseInt).collect(Collectors.toSet());
            List<FoodItem> filteredFavorites = foodItems.stream().filter(e -> favoritesList.contains(e.id)).collect(Collectors.toList());
            mAdapter.updateList(filteredFavorites);
            filteringFavorites = true;
        } else {
            mAdapter.updateList(foodItems);
            filteringFavorites = false;
        }
    }
    private void filterFavorites(boolean set){
        if(!mSearchView.isActivated()){
            mSearchView.setQuery("", true);
            mSearchView.onActionViewCollapsed();
        }
        if(set){
            Set<Integer> favoritesList = Arrays.stream(prefs.getString("Favorites", "").split(",")).filter(StringUtils::isNumeric).map(Integer::parseInt).collect(Collectors.toSet());
            List<FoodItem> filteredFavorites = foodItems.stream().filter(e -> favoritesList.contains(e.id)).collect(Collectors.toList());
            mAdapter.updateList(filteredFavorites);
            filteringFavorites = true;
        } else {
            mAdapter.updateList(foodItems);
            filteringFavorites = false;
        }
    }
    // Suppressing this warning because we're not using all of the constants for the night mode setting
    @SuppressLint("SwitchIntDef")
    public void cycleDayNight(){
        int currentMode = AppCompatDelegate.getDefaultNightMode();
        SharedPreferences.Editor prefEditor = prefs.edit();
        switch (currentMode){
            case AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM:
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                prefEditor.putInt("DayNightMode", AppCompatDelegate.MODE_NIGHT_NO);
                Toast.makeText(this, getString(R.string.dark_mode_no), Toast.LENGTH_SHORT).show();
                break;
            case AppCompatDelegate.MODE_NIGHT_NO:
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                prefEditor.putInt("DayNightMode", AppCompatDelegate.MODE_NIGHT_YES);
                Toast.makeText(this, getString(R.string.dark_mode_yes), Toast.LENGTH_SHORT).show();
                break;
            default:
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
                prefEditor.putInt("DayNightMode", AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
                Toast.makeText(this, getString(R.string.dark_mode_system), Toast.LENGTH_SHORT).show();
                break;

        }
        prefEditor.apply();
    }
}