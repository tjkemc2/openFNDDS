package com.tjk104.openfndds;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.tjk104.openfndds.database.FoodDatabase;
import com.tjk104.openfndds.database.FoodItem;
import com.tjk104.openfndds.database.FoodItemDao;
import com.tjk104.openfndds.database.FoodNutrient;
import com.tjk104.openfndds.database.FoodNutrientDao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

public class ComparisonActivity extends AppCompatActivity {
    private FoodNutrientDao mFoodNutrientDao;
    private FoodItem mLeftFoodItem;
    private FoodItem mRightFoodItem;
    private List<FoodItem> mAllFoodItems = new ArrayList<>();
    private List<FoodNutrient> mLeftNutrients = new ArrayList<>();
    private List<FoodNutrient> mRightNutrients = new ArrayList<>();

    private ComparisonListAdapter mComparisonListAdapter;

    private EditText mLeftSearchBox;
    private TextView mLeftId;
    private Spinner mLeftSpinner;
    private ArrayAdapter<FoodItem> mLeftSpinnerAdapter;

    private EditText mRightSearchBox;
    private TextView mRightId;
    private Spinner mRightSpinner;
    private ArrayAdapter<FoodItem> mRightSpinnerAdapter;

    private static final int NUM_THREADS = 4;
    private final ExecutorService executorService = Executors.newFixedThreadPool(NUM_THREADS);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FoodDatabase db = FoodDatabase.getDatabase(getApplicationContext());
        FoodItemDao mFoodItemDao = db.foodItemDao();
        mFoodNutrientDao = db.foodNutrientDao();
        setContentView(R.layout.activity_comparison);
        View loadingOverlay = findViewById(R.id.comparison_loading_overlay);
        loadingOverlay.setVisibility(View.VISIBLE);

        mLeftSearchBox = findViewById(R.id.comparison_search_box_left);
        Button mLeftSearchButton = findViewById(R.id.comparison_search_button_left);
        mLeftId = findViewById(R.id.food_id_left);
        mLeftSearchButton.setOnClickListener(this::searchLeft);
        mLeftSearchBox.setOnEditorActionListener((v, v2, v3) -> {
            searchLeft(v);
            return true;
        });

        mRightSearchBox = findViewById(R.id.comparison_search_box_right);
        Button mRightSearchButton = findViewById(R.id.comparison_search_button_right);
        mRightId = findViewById(R.id.food_id_right);
        mRightSearchButton.setOnClickListener(this::searchRight);
        mRightSearchBox.setOnEditorActionListener((v, v2, v3) -> {
            searchRight(v);
            return true;
        });

        RecyclerView mComparisonRecyclerView = findViewById(R.id.comparison_recycler_view);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        mComparisonListAdapter = new ComparisonListAdapter(this);
        mComparisonRecyclerView.setLayoutManager(mLayoutManager);
        mComparisonRecyclerView.setAdapter(mComparisonListAdapter);

        mLeftSpinner = findViewById(R.id.comparison_spinner_left);
        mLeftSpinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item);
        mLeftSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mLeftSpinner.setAdapter(mLeftSpinnerAdapter);
        mLeftSpinner.setOnItemSelectedListener(getLeftSpinnerListener());
        mLeftId.setText(R.string.no_item_selected);

        mRightSpinner = findViewById(R.id.comparison_spinner_right);
        mRightSpinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item);
        mRightSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mRightSpinner.setAdapter(mRightSpinnerAdapter);
        mRightSpinner.setOnItemSelectedListener(getRightSpinnerListener());
        mRightId.setText(R.string.no_item_selected);


        executorService.execute(() -> {
            mAllFoodItems = mFoodItemDao.getAll();
            runOnUiThread(() -> {
                mLeftSpinnerAdapter.clear();
                mLeftSpinnerAdapter.addAll(mAllFoodItems);
                mRightSpinnerAdapter.clear();
                mRightSpinnerAdapter.addAll(mAllFoodItems);
                loadingOverlay.setVisibility(View.GONE);
            });
        });
    }

    private AdapterView.OnItemSelectedListener getLeftSpinnerListener() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                FoodItem selected = (FoodItem) parent.getSelectedItem();
                setLeft(selected);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                setLeft(null);
            }
        };
    }

    private AdapterView.OnItemSelectedListener getRightSpinnerListener() {
        return new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                FoodItem selected = (FoodItem) parent.getSelectedItem();
                setRight(selected);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                setRight(null);
            }
        };
    }

    private void updateComparison(){
        mComparisonListAdapter.updateList(mLeftNutrients, mRightNutrients);
    }

    private void setLeft(FoodItem selected){
        if(selected != null){
            executorService.execute(() -> {
                this.mLeftFoodItem = selected;
                this.mLeftNutrients = mFoodNutrientDao.getFoodNutrients(selected.id);
                runOnUiThread(() -> {
                    updateComparison();
                    mLeftId.setText(getString(R.string.food_id, mLeftFoodItem.id));
                });
            });
        } else {
            this.mLeftFoodItem = null;
            this.mLeftNutrients = new ArrayList<>();
            updateComparison();
            mLeftId.setText(R.string.no_item_selected);
        }
    }

    private void setRight(FoodItem selected){
        if(selected != null){
            executorService.execute(() -> {
                this.mRightFoodItem = selected;
                this.mRightNutrients = mFoodNutrientDao.getFoodNutrients(selected.id);
                runOnUiThread(() -> {
                    updateComparison();
                    mRightId.setText(getString(R.string.food_id, mRightFoodItem.id));
                });
            });
        } else {
            this.mRightFoodItem = null;
            this.mRightNutrients = new ArrayList<>();
            updateComparison();
            mRightId.setText(R.string.no_item_selected);
        }
    }

    private void searchLeft(View v){
        mLeftSpinnerAdapter.clear();
        String query = mLeftSearchBox.getText().toString();
        List<FoodItem> searchResults = mAllFoodItems.stream()
                .filter(e -> {
                    String name = e.name.toLowerCase();
                    return Arrays.stream(query.toLowerCase().split("\\s+")).allMatch(name::contains);
                }).collect(Collectors.toList());
        mLeftSpinner.setSelected(false);
        mLeftSpinnerAdapter.addAll(searchResults);
        mLeftSpinnerAdapter.notifyDataSetChanged();
        mLeftSpinner.performClick();
    }

    private void searchRight(View v){
        mRightSpinnerAdapter.clear();
        String query = mRightSearchBox.getText().toString();
        List<FoodItem> searchResults = mAllFoodItems.stream()
                .filter(e -> {
                    String name = e.name.toLowerCase();
                    return Arrays.stream(query.toLowerCase().split("\\s+")).allMatch(name::contains);
                }).collect(Collectors.toList());
        mRightSpinner.setSelected(false);
        mRightSpinnerAdapter.addAll(searchResults);
        mRightSpinnerAdapter.notifyDataSetChanged();
        mRightSpinner.performClick();

    }
}