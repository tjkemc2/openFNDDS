package com.tjk104.openfndds;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.tjk104.openfndds.database.FoodItem;

import java.util.List;

public class FoodListAdapter extends RecyclerView.Adapter<FoodListAdapter.FoodListViewHolder> {
    private List<FoodItem> mItems;
    private final LayoutInflater mInflater;

    public class FoodListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private final TextView foodName;
        private final TextView foodId;
        public FoodListViewHolder(@NonNull LinearLayout v) {
            super(v);
            foodName = v.findViewById(R.id.food_name);
            foodId = v.findViewById(R.id.food_id);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View v){
            int position = getLayoutPosition();
            FoodItem item = mItems.get(position);
            Intent intent = new Intent(v.getContext(), FoodDetailActivity.class);
            intent.putExtra(FoodDetailActivity.INCOMING_FOOD_ITEM, item.id);
            v.getContext().startActivity(intent);
        }

        public void setFoodName(String name){
            foodName.setText(name);
        }

        public void setFoodId(String id){
            foodId.setText(id);
        }

    }

    FoodListAdapter(Context context){
        mInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public FoodListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LinearLayout listView = (LinearLayout) mInflater.inflate(R.layout.food_item, parent, false);
        return new FoodListViewHolder(listView);
    }

    @Override
    public void onBindViewHolder(@NonNull FoodListViewHolder holder, int position) {
        holder.setFoodName(mItems.get(position).name);
        holder.setFoodId(MainActivity.getContext().getResources().getString(R.string.food_id, mItems.get(position).id));
    }

    @Override
    public int getItemCount() {
        return mItems == null ? 0 : mItems.size();
    }

    public void updateList(List<FoodItem> foodItems){
        mItems = foodItems;
        notifyDataSetChanged();
    }
}
