package com.tjk104.openfndds.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface FoodNutrientDao {
    @Query("SELECT * FROM food_nutrients WHERE foodId = :foodId")
    List<FoodNutrient> getFoodNutrients(int foodId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<FoodNutrient> nutrients);
}
