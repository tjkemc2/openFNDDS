package com.tjk104.openfndds.database;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface FoodPortionDao {
    @Query("SELECT * FROM food_portions WHERE foodId = :foodId ORDER BY gramWeight DESC")
    List<FoodPortion> getFoodPortions(int foodId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(List<FoodPortion> portions);
}
