package com.tjk104.openfndds.database;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.tjk104.openfndds.MainActivity;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Database(entities = {FoodItem.class, FoodNutrient.class, FoodPortion.class}, version = 1, exportSchema = false)
public abstract class FoodDatabase extends RoomDatabase {
    public static volatile FoodDatabase INSTANCE;
    public abstract FoodItemDao foodItemDao();
    public abstract FoodNutrientDao foodNutrientDao();
    public abstract FoodPortionDao foodPortionDao();

    private static final String DB_LOG_TAG = "DatabaseWriter";
    private static final int NUMBER_OF_THREADS = 4;
    private static ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public static FoodDatabase getDatabase(final Context context){
        if(INSTANCE == null){
            synchronized (FoodDatabase.class){
                INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                        FoodDatabase.class,
                        "food_items").addCallback(sOnCreateCallback).fallbackToDestructiveMigration().build();
            }
        }
        return INSTANCE;
    }

    private static final RoomDatabase.Callback sOnCreateCallback = new RoomDatabase.Callback(){
        @Override
        public void onCreate(@NonNull SupportSQLiteDatabase db) {
            databaseWriteExecutor.execute(() -> {
                INSTANCE.clearAllTables();
                FoodItemDao foodItemDao = INSTANCE.foodItemDao();
                Log.d(DB_LOG_TAG, "Writing Table food_items");
                List<FoodItem> newItems = FileReader.readFoodItems();
                MainActivity.updateLoadingBar("Writing Food Items to Database...");
                foodItemDao.insertAll(newItems);
                FoodNutrientDao foodNutrientDao = INSTANCE.foodNutrientDao();
                Log.d(DB_LOG_TAG, "Writing Table food_nutrients");
                List<FoodNutrient> newNutrients = FileReader.readFoodNutrients();
                MainActivity.updateLoadingBar("Writing Food Nutrients to Database...");
                foodNutrientDao.insertAll(newNutrients);
                FoodPortionDao foodPortionDao = INSTANCE.foodPortionDao();
                Log.d(DB_LOG_TAG, "Writing table food_portions");
                List<FoodPortion> newPortions = FileReader.readFoodPortions();
                MainActivity.updateLoadingBar("Writing Portion Sizes to Database...");
                foodPortionDao.insertAll(newPortions);
            });
        }

        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            MainActivity.updateLoadingBar("Opening Database...");
        }
    };

    public void waitForExecutor() throws InterruptedException {
        if(!databaseWriteExecutor.isTerminated()){
            databaseWriteExecutor.shutdown();
            databaseWriteExecutor.awaitTermination(5, TimeUnit.MINUTES);
            databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);
        }
    }
}
