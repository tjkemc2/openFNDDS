package com.tjk104.openfndds.database;

import android.content.res.Resources;

import com.opencsv.CSVReader;
import com.tjk104.openfndds.MainActivity;
import com.tjk104.openfndds.R;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

public class FileReader {
    private static class NutrientEntry{
        public final int id;
        public final String name;
        public final String unitName;
        public NutrientEntry(int id, String name, String unitName){
            this.id = id;
            this.name = name;
            this.unitName = unitName;
        }
    }
    public static List<FoodItem> readFoodItems(){
        Resources res = MainActivity.getContext().getResources();
        List<FoodItem> results = new ArrayList<>();
        try(CSVReader reader = new CSVReader(new InputStreamReader(res.openRawResource(R.raw.food)))){
            reader.skip(1);
            String[] nextLine;
            while((nextLine = reader.readNext()) != null){
                if(reader.getLinesRead() % 100 == 0){
                    MainActivity.updateLoadingBar(String.format(Locale.ENGLISH, "Parsing Food Items: %d", (int) reader.getLinesRead()));
                }
                FoodItem newItem = new FoodItem(Integer.parseInt(nextLine[0]), nextLine[2]);
                results.add(newItem);
            }
            MainActivity.updateLoadingBar(String.format(Locale.ENGLISH, "Parsing Food Items: %d", (int) reader.getLinesRead()));
        } catch (Exception ignored) {
            // Ignoring this exception...
        }



        return results;
    }

    public static List<FoodNutrient> readFoodNutrients(){
        Resources res = MainActivity.getContext().getResources();
        List<FoodNutrient> results = new ArrayList<>();
        List<NutrientEntry> nutrientEntries = new ArrayList<>();
        try(CSVReader nutrientEntryReader = new CSVReader(new InputStreamReader(res.openRawResource(R.raw.nutrient)))){
            nutrientEntryReader.skip(1);
            String[] nextNutrientEntry;
            while ((nextNutrientEntry = nutrientEntryReader.readNext()) != null) {
                if(nutrientEntryReader.getLinesRead() % 10 == 0){
                    MainActivity.updateLoadingBar(String.format(Locale.ENGLISH, "Parsing Nutrients: %d", (int) nutrientEntryReader.getLinesRead()));
                }
                if(!nextNutrientEntry[1].matches("^\\d+:\\d+.*")){
                    NutrientEntry newEntry = new NutrientEntry(Integer.parseInt(nextNutrientEntry[0]), nextNutrientEntry[1], nextNutrientEntry[2]);
                    nutrientEntries.add(newEntry);
                }
            }
            MainActivity.updateLoadingBar(String.format(Locale.ENGLISH, "Parsing Nutrients: %d", (int) nutrientEntryReader.getLinesRead()));

        } catch(Exception ignored){
            ///Ignoring this exception...
        }
        try(CSVReader nutrientReader = new CSVReader(new InputStreamReader(res.openRawResource(R.raw.food_nutrient)))) {
            nutrientReader.skip(1);
            String[] nextNutrient;
            while ((nextNutrient = nutrientReader.readNext()) != null) {
                if (nutrientReader.getLinesRead() % 1000 == 0) {
                    MainActivity.updateLoadingBar(String.format(Locale.ENGLISH, "Parsing Food Nutrients: %d", (int) nutrientReader.getLinesRead()));
                }
                int id = Integer.parseInt(nextNutrient[0]);
                int foodId = Integer.parseInt(nextNutrient[1]);
                int nutrientId = Integer.parseInt(nextNutrient[2]);
                float amount = Float.parseFloat(nextNutrient[3]);
                Optional<NutrientEntry> match = nutrientEntries.stream().filter(e -> e.id == nutrientId).findFirst();
                if (match.isPresent()) {
                    FoodNutrient newNutrient = new FoodNutrient(id, foodId, match.get().name, amount, match.get().unitName);
                    results.add(newNutrient);
                }
            }
            MainActivity.updateLoadingBar(String.format(Locale.ENGLISH, "Parsing Food Nutrients: %d", (int) nutrientReader.getLinesRead()));
        } catch(Exception ignored){
            //Ignoring this exception...
        }
        return results;
    }

    public static List<FoodPortion> readFoodPortions() {
        Resources res = MainActivity.getContext().getResources();
        List<FoodPortion> results = new ArrayList<>();
        try(CSVReader portionReader = new CSVReader(new InputStreamReader(res.openRawResource(R.raw.food_portion)))) {
            portionReader.skip(1);
            String[] nextPortion;
            while((nextPortion = portionReader.readNext()) != null){
                if(portionReader.getLinesRead() % 1000 == 0){
                    MainActivity.updateLoadingBar(String.format(Locale.ENGLISH, "Parsing Portion Sizes: %d", (int) portionReader.getLinesRead()));
                }
                int id = Integer.parseInt(nextPortion[0]);
                int foodId = Integer.parseInt(nextPortion[1]);
                String description = nextPortion[5];
                float gramWeight = Float.parseFloat(nextPortion[7]);
                FoodPortion newPortion = new FoodPortion(id, foodId, description, gramWeight);
                results.add(newPortion);
            }
            MainActivity.updateLoadingBar(String.format(Locale.ENGLISH, "Parsing Portion Sizes: %d", (int) portionReader.getLinesRead()));
        } catch(Exception ignored){
            //Ignoring this exception
        }
        return results;
    }
}
