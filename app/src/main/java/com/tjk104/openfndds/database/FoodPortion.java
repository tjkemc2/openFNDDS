package com.tjk104.openfndds.database;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import java.util.Locale;

@Entity(tableName = "food_portions",
        foreignKeys = @ForeignKey(entity = FoodItem.class, parentColumns = "id", childColumns = "foodId"),
        indices = @Index("foodId"))
public class FoodPortion {
    @PrimaryKey
    public final int id;
    public final int foodId;
    public final String description;
    public final float gramWeight;

    public FoodPortion(int id, int foodId, String description, float gramWeight){
        this.id = id;
        this.foodId = foodId;
        this.description = description;
        this.gramWeight = gramWeight;
    }

    @NonNull
    @Override
    public String toString() {
        return String.format(Locale.ENGLISH, "%s (%.2f)", this.description, this.gramWeight);
    }

    public static final FoodPortion basePortion = new FoodPortion(0, 0, "100g", 100);
}
